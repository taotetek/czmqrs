#[link(name = "czmq")]

extern "C" {

    // zsock

    pub fn zsock_new(_type: ::std::os::raw::c_int) -> *mut zsock_t;

    pub fn zsock_destroy(self_p: *mut *mut zsock_t);

    pub fn zsock_bind(
        _self: *mut zsock_t,
        format: *const ::std::os::raw::c_char,
        ...
    ) -> ::std::os::raw::c_int;

    pub fn zsock_unbind(
        _self: *mut zsock_t,
        format: *const ::std::os::raw::c_char,
        ...
    ) -> ::std::os::raw::c_int;

    pub fn zsock_connect(
        _self: *mut zsock_t,
        format: *const ::std::os::raw::c_char,
        ...
    ) -> ::std::os::raw::c_int;

    pub fn zsock_type_str(_self: *mut zsock_t) -> *const ::std::os::raw::c_char;

    pub fn zsock_endpoint(_self: *mut zsock_t) -> *const ::std::os::raw::c_char;

    // zstr

    pub fn zstr_recv(source: *mut ::std::os::raw::c_void) -> *mut ::std::os::raw::c_char;

    pub fn zstr_send(
        dest: *mut ::std::os::raw::c_void,
        string: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;

    // zsys

    pub fn zsys_hostname() -> *mut ::std::os::raw::c_char;

    pub fn zsys_version(
        major: *mut ::std::os::raw::c_int,
        minor: *mut ::std::os::raw::c_int,
        patch: *mut ::std::os::raw::c_int,
    );

    pub fn zsys_init() -> *mut ::std::os::raw::c_void;

    // zframe

    pub fn zframe_new(data: *const ::std::os::raw::c_void, size: u64) -> *mut zframe_t;

    pub fn zframe_new_empty() -> *mut zframe_t;

    pub fn zframe_reset(_self: *mut zframe_t, data: *const ::std::os::raw::c_void, size: u64);

    pub fn zframe_from(string: *const ::std::os::raw::c_char) -> *mut zframe_t;

    pub fn zframe_data(_self: *mut zframe_t) -> *mut ::std::os::raw::c_uchar;

    pub fn zframe_strhex(_self: *mut zframe_t) -> *mut ::std::os::raw::c_char;

    pub fn zframe_streq(_self: *mut zframe_t, string: *const ::std::os::raw::c_char) -> u8;

    pub fn zframe_more(_self: *mut zframe_t) -> ::std::os::raw::c_int;

    pub fn zframe_set_more(_self: *mut zframe_t, more: ::std::os::raw::c_int);

    pub fn zframe_recv(source: *mut ::std::os::raw::c_void) -> *mut zframe_t;

    pub fn zframe_dup(_self: *mut zframe_t) -> *mut zframe_t;

    pub fn zframe_eq(_self: *mut zframe_t, other: *mut zframe_t) -> u8;

    pub fn zframe_send(
        self_p: *mut *mut zframe_t,
        dest: *mut ::std::os::raw::c_void,
        flags: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;

    pub fn zframe_size(_self: *mut zframe_t) -> u64;

    pub fn zframe_meta(
        _self: *mut zframe_t,
        property: *const ::std::os::raw::c_char,
    ) -> *const ::std::os::raw::c_char;

    pub fn zframe_destroy(self_p: *mut *mut zframe_t);

    // zmsg

    pub fn zmsg_new() -> *mut zmsg_t;

    pub fn zmsg_send(
        self_p: *mut *mut zmsg_t,
        dest: *mut ::std::os::raw::c_void,
    ) -> ::std::os::raw::c_int;

    pub fn zmsg_recv(source: *mut ::std::os::raw::c_void) -> *mut zmsg_t;

    pub fn zmsg_new_signal(status: ::std::os::raw::c_uchar) -> *mut zmsg_t;

    pub fn zmsg_decode(frame: *mut zframe_t) -> *mut zmsg_t;

    pub fn zmsg_sendm(self_p: *mut *mut zmsg_t, dest: *mut ::std::os::raw::c_void);

    pub fn zmsg_destroy(self_p: *mut *mut zmsg_t);

    pub fn zmsg_pop(_self: *mut zmsg_t) -> *mut zframe_t;

    pub fn zmsg_size(_self: *mut zmsg_t) -> u64;

    pub fn zmsg_content_size(_self: *mut zmsg_t) -> u64;

    pub fn zmsg_prepend(_self: *mut zmsg_t, frame_p: *mut *mut zframe_t) -> ::std::os::raw::c_int;

    pub fn zmsg_append(_self: *mut zmsg_t, frame_p: *mut *mut zframe_t) -> ::std::os::raw::c_int;

    pub fn zmsg_pushstr(
        _self: *mut zmsg_t,
        string: *const ::std::os::raw::c_char,
    ) -> ::std::os::raw::c_int;

    pub fn zmsg_addstr(_self: *mut zmsg_t, string: *const ::std::os::raw::c_char);

    pub fn zmsg_popstr(_self: *mut zmsg_t) -> *mut ::std::os::raw::c_char;

    // zpoller

    pub fn zpoller_new(reader: *mut ::std::os::raw::c_void, ...) -> *mut zpoller_t;

    pub fn zpoller_destroy(self_p: *mut *mut zpoller_t);

    pub fn zpoller_add(
        _self: *mut zpoller_t,
        reader: *mut ::std::os::raw::c_void,
    ) -> ::std::os::raw::c_int;

    pub fn zpoller_remove(
        _self: *mut zpoller_t,
        reader: *mut ::std::os::raw::c_void,
    ) -> ::std::os::raw::c_int;

    pub fn zpoller_wait(
        _self: *mut zpoller_t,
        timeout: ::std::os::raw::c_int,
    ) -> *mut ::std::os::raw::c_void;

    pub fn zpoller_expired(_self: *mut zpoller_t) -> u8;

    pub fn zpoller_terminated(_self: *mut zpoller_t) -> u8;

    pub fn zpoller_set_nonstop(_self: *mut zpoller_t, nonstop: u8);
}

pub enum Struct__zsock_t {}
pub type zsock_t = Struct__zsock_t;

pub enum Struct__zframe_t {}
pub type zframe_t = Struct__zframe_t;

pub enum Struct__zmsg_t {}
pub type zmsg_t = Struct__zmsg_t;

pub enum Struct__zpoller_t {}
pub type zpoller_t = Struct__zpoller_t;
