use std::error;
use std::ffi::{IntoStringError, NulError};
use std::fmt;
use std::num::ParseIntError;
use std::str::Utf8Error;
use std::string::FromUtf8Error;

#[derive(Debug)]
pub enum Error {
    FromUtf8Error(FromUtf8Error),
    Utf8Error(Utf8Error),
    Internal(ErrorKind),
    IntoStringError(IntoStringError),
    NulError(NulError),
    ParseIntError(ParseIntError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Internal(ref err) => write!(f, "internal czmq error: {}", err),
            Error::FromUtf8Error(ref err) => write!(f, "{}", err),
            Error::Utf8Error(ref err) => write!(f, "{}", err),
            Error::IntoStringError(ref err) => write!(f, "{}", err),
            Error::NulError(ref err) => write!(f, "{}", err),
            Error::ParseIntError(ref err) => write!(f, "{}", err),
        }
    }
}
impl error::Error for Error {
    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::Internal(ref err) => Some(err),
            Error::FromUtf8Error(ref err) => Some(err),
            Error::Utf8Error(ref err) => Some(err),
            Error::IntoStringError(ref err) => Some(err),
            Error::NulError(ref err) => Some(err),
            Error::ParseIntError(ref err) => Some(err),
        }
    }
}

#[derive(Debug)]
pub enum ErrorKind {
    CZMQError,
    NulPtrError,
    ConnectError,
    BindError,
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ErrorKind::CZMQError => write!(f, "czmq error"),
            ErrorKind::NulPtrError => write!(f, "null pointer returned from czmq"),
            ErrorKind::BindError => write!(f, "bind error"),
            ErrorKind::ConnectError => write!(f, "connect error"),
        }
    }
}

impl error::Error for ErrorKind {
    fn cause(&self) -> Option<&error::Error> {
        None
    }
}

impl From<NulError> for Error {
    fn from(error: NulError) -> Self {
        Error::NulError(error)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(error: FromUtf8Error) -> Self {
        Error::FromUtf8Error(error)
    }
}

impl From<Utf8Error> for Error {
    fn from(error: Utf8Error) -> Self {
        Error::Utf8Error(error)
    }
}

impl From<IntoStringError> for Error {
    fn from(error: IntoStringError) -> Self {
        Error::IntoStringError(error)
    }
}

impl From<ParseIntError> for Error {
    fn from(error: ParseIntError) -> Self {
        Error::ParseIntError(error)
    }
}
