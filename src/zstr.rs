extern crate czmq_sys;
extern crate libc;

use std::str;
use Error;
use ZFrame;
use ZSock;

pub fn zstr_send(dest: &ZSock, s: &str) -> Result<i32, Error> {
    let data = s.as_bytes();
    let frame = ZFrame::new(data);
    let rc = frame.send(&dest, None)?;
    Ok(rc)
}
