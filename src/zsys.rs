extern crate czmq_sys;
extern crate libc;

use std::ffi::CStr;
use std::str;
use std::sync::{Once, ONCE_INIT};

static CZMQ_INIT: Once = ONCE_INIT;

#[derive(Debug)]
pub struct ZSys;

impl ZSys {
    pub fn init() {
        CZMQ_INIT.call_once(|| {
            unsafe { czmq_sys::zsys_init() };
        });
    }

    pub fn hostname() -> Option<String> {
        unsafe {
            let ptr = czmq_sys::zsys_hostname();
            let bytes = CStr::from_ptr(ptr).to_bytes();
            let hostname = str::from_utf8(bytes);
            libc::free(ptr as *mut libc::c_void);
            match hostname {
                Ok(v) => Some(v.to_string()),
                Err(_) => None,
            }
        }
    }

    pub fn version() -> (i32, i32, i32) {
        let mut major = 0;
        let mut minor = 0;
        let mut patch = 0;

        unsafe {
            czmq_sys::zsys_version(&mut major, &mut minor, &mut patch);
        }

        (major as i32, minor as i32, patch as i32)
    }
}

#[cfg(test)]
mod tests {
    use ZSys;

    #[test]
    fn test_hostname() {
        let host = ZSys::hostname();
        assert!(host.is_some());
    }

    #[test]
    fn test_version() {
        let (major, minor, patch) = ZSys::version();
        assert_eq!(major, 4);
        assert_eq!(minor, 1);
        assert_eq!(patch, 0);
    }
}
