extern crate czmq_sys;
extern crate libc;

use std::ffi::{CStr, CString};
use std::os::raw::c_void;
use std::{ptr, str};
use Error;
use ErrorKind;
use ZSys;

pub enum ZSockType {
    PAIR = 0,
    PUB = 1,
    SUB = 2,
    REQ = 3,
    REP = 4,
    DEALER = 5,
    ROUTER = 6,
    PULL = 7,
    PUSH = 8,
    XPUB = 9,
    XSUB = 10,
    STREAM = 11,
}

#[derive(Debug)]
pub struct ZSock {
    pub zsock: *mut czmq_sys::zsock_t,
    zsock_owned: bool,
}

impl Drop for ZSock {
    fn drop(&mut self) {
        if self.zsock_owned {
            unsafe { czmq_sys::zsock_destroy(&mut self.zsock) };
        }
    }
}

impl PartialEq for ZSock {
    fn eq(&self, other: &ZSock) -> bool {
        self.zsock == other.zsock
    }
}

impl ZSock {
    pub fn new(zsock_type: ZSockType) -> ZSock {
        ZSys::init();
        ZSock {
            zsock: unsafe { czmq_sys::zsock_new(zsock_type as i32) },
            zsock_owned: true,
        }
    }

    pub fn from_mut_ptr(ptr: *mut c_void) -> ZSock {
        ZSock {
            zsock: ptr as *mut czmq_sys::zsock_t,
            zsock_owned: false,
        }
    }

    pub fn as_mut_ptr(&mut self) -> *mut c_void {
        self.zsock as *mut c_void
    }

    pub fn bind(&self, endpoint: &str) -> Result<i32, Error> {
        let ep = CString::new(endpoint)?;
        let rc = unsafe {
            czmq_sys::zsock_bind(
                self.zsock,
                "%s\0".as_ptr() as *const i8,
                ep.as_ptr() as *const i8,
            )
        };
        if rc == -1 {
            Err(Error::Internal(ErrorKind::BindError))
        } else {
            Ok(rc)
        }
    }

    pub fn unbind(&self, endpoint: &str) -> Result<i32, Error> {
        let ep = CString::new(endpoint)?;
        let rc = unsafe {
            czmq_sys::zsock_unbind(
                self.zsock,
                "%s\0".as_ptr() as *const i8,
                ep.as_ptr() as *const i8,
            )
        };
        if rc == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(rc)
        }
    }

    pub fn connect(&self, endpoint: &str) -> Result<i32, Error> {
        let ep = CString::new(endpoint)?;
        let rc = unsafe {
            czmq_sys::zsock_connect(
                self.zsock,
                "%s\0".as_ptr() as *const i8,
                ep.as_ptr() as *const i8,
            )
        };
        if rc == -1 {
            Err(Error::Internal(ErrorKind::ConnectError))
        } else {
            Ok(rc)
        }
    }

    pub fn type_str(&self) -> Result<&str, Error> {
        let type_ptr = unsafe { czmq_sys::zsock_type_str(self.zsock) };
        let c_str = unsafe { CStr::from_ptr(type_ptr) };
        let type_str = c_str.to_str()?;
        Ok(type_str)
    }

    pub fn endpoint(&self) -> Result<&str, Error> {
        let endpoint_ptr = unsafe { czmq_sys::zsock_endpoint(self.zsock) };
        if endpoint_ptr == ptr::null() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            let endpoint_cstr = unsafe { CStr::from_ptr(endpoint_ptr) };
            let endpoint_str = endpoint_cstr.to_str()?;
            Ok(endpoint_str)
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{thread, time};
    use {zstr_send, Error, ZMsg, ZSock, ZSockType};

    #[test]
    fn test_zsock() -> Result<(), Error> {
        let writer = ZSock::new(ZSockType::PUSH);
        let port = writer.bind("tcp://127.0.0.1:*")?;
        assert!(port != -1);

        let type_str = writer.type_str()?;
        assert_eq!(type_str, "PUSH");

        let endpoint = format!("tcp://127.0.0.1:{}", port);
        let rc = writer.unbind(&endpoint)?;
        assert_eq!(rc, 0);

        // in some cases and especially when running under valgrind,
        // doing a bind immediately after an unbind causes an
        // EADDRINUSE error. Short sleep to allow the OS to
        // release the port.
        thread::sleep(time::Duration::from_millis(100));

        // Bind again
        let rc = writer.bind(&endpoint)?;
        assert_eq!(rc, port);

        let reader = ZSock::new(ZSockType::PULL);
        let rc = reader.connect(&endpoint)?;
        assert!(rc != -1);

        let endpoint = format!("tcp://127.0.0.1:{}", port);
        assert_eq!(writer.endpoint()?, endpoint);

        let type_str = reader.type_str()?;
        assert_eq!(type_str, "PULL");

        let s = "Hello World";
        let rc = zstr_send(&writer, &s)?;
        assert!(rc == 0);

        let msg = ZMsg::recv(&reader)?;
        let msg_string = msg.popstr()?;
        assert_eq!(msg_string, "Hello World");

        Ok(())
    }
}
