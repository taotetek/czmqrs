extern crate czmq_sys;
extern crate libc;

use std::ptr;

use std::os::raw::{c_int, c_void};
use Error;
use ErrorKind;
use ZSock;

#[derive(Debug)]
pub struct ZPoller {
    zpoller: *mut czmq_sys::zpoller_t,
}

impl Drop for ZPoller {
    fn drop(&mut self) {
        unsafe { czmq_sys::zpoller_destroy(&mut self.zpoller) };
    }
}

impl ZPoller {
    pub fn new() -> Result<ZPoller, Error> {
        let zpoller = unsafe { czmq_sys::zpoller_new(ptr::null_mut()) };

        if zpoller == ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            Ok(ZPoller { zpoller: zpoller })
        }
    }

    pub fn add(&mut self, reader: &mut ZSock) -> Result<(), Error> {
        let rc = unsafe { czmq_sys::zpoller_add(self.zpoller, reader.as_mut_ptr()) };

        if rc == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(())
        }
    }

    pub fn remove(&mut self, reader: &mut ZSock) -> Result<(), Error> {
        let rc = unsafe { czmq_sys::zpoller_remove(self.zpoller, reader.zsock as *mut c_void) };

        if rc == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(())
        }
    }

    pub fn wait(&mut self, timeout: Option<u32>) -> Option<ZSock> {
        let t = match timeout {
            Some(time) => time as c_int,
            None => -1 as c_int,
        };

        let ptr = unsafe { czmq_sys::zpoller_wait(self.zpoller, t) };

        if ptr == ptr::null_mut() {
            None
        } else {
            Some(ZSock::from_mut_ptr(ptr))
        }
    }

    pub fn expired(&self) -> bool {
        unsafe { czmq_sys::zpoller_expired(self.zpoller) == 1 }
    }

    pub fn terminated(&self) -> bool {
        unsafe { czmq_sys::zpoller_terminated(self.zpoller) == 1 }
    }

    pub fn set_nonstop(&self, nonstop: bool) {
        let c_nonstop = if nonstop { 1 } else { 0 };
        unsafe { czmq_sys::zpoller_set_nonstop(self.zpoller, c_nonstop) }
    }
}

#[cfg(test)]
mod tests {
    use {Error, ErrorKind, ZFrame, ZPoller, ZSock, ZSockType};

    #[test]
    fn test_zpoller() -> Result<(), Error> {
        let mut poller = ZPoller::new()?;
        let vent = ZSock::new(ZSockType::PUSH);
        let vent_port = vent.bind("tcp://127.0.0.1:*")?;

        let mut sink = ZSock::new(ZSockType::PULL);
        let _rc = sink.connect(&format!("tcp://127.0.0.1:{}", vent_port))?;

        let mut bowl = ZSock::new(ZSockType::PULL);
        let _rc = bowl.connect(&format!("tcp://127.0.0.1:{}", vent_port))?;

        let mut dish = ZSock::new(ZSockType::PULL);
        let _rc = dish.connect(&format!("tcp://127.0.0.1:{}", vent_port))?;
        poller.add(&mut bowl)?;
        poller.add(&mut dish)?;
        poller.add(&mut sink)?;

        let frame = ZFrame::from("Hello World");
        let _rc = frame.send(&vent, None)?;
        let which = poller.wait(None);
        match which {
            Some(s) => {
                let frame = ZFrame::recv(&s)?;
                assert!(frame.streq("Hello World")?);
            }
            None => {
                return Err(Error::Internal(ErrorKind::CZMQError));
            }
        }

        assert_eq!(poller.expired(), false);
        assert_eq!(poller.terminated(), false);

        Ok(())
    }
}
