extern crate czmq_sys;
extern crate libc;

use std::ffi::CStr;
use std::os::raw::c_void;
use std::ptr;
use Error;
use ErrorKind;
use ZSock;

bitflags! {
    pub struct Flags: i32 {
        const ZFRAME_MORE = 0b00000001;
        const ZFRAME_REUSE = 0b00000010;
        const ZFRAME_DONTWAIT = 0b00000100;
    }
}

#[derive(Debug)]
pub struct ZMsg {
    zmsg: *mut czmq_sys::zmsg_t,
}

impl Drop for ZMsg {
    fn drop(&mut self) {
        unsafe { czmq_sys::zmsg_destroy(&mut self.zmsg) };
    }
}

impl ZMsg {
    pub fn new() -> ZMsg {
        ZMsg {
            zmsg: unsafe { czmq_sys::zmsg_new() },
        }
    }

    pub fn send(self, dest: &ZSock) -> Result<i32, Error> {
        let mut zmsg = self;
        let rc = unsafe { czmq_sys::zmsg_send(&mut zmsg.zmsg, dest.zsock as *mut c_void) };
        if rc == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(rc)
        }
    }

    pub fn recv(source: &ZSock) -> Result<ZMsg, Error> {
        let msg = unsafe { czmq_sys::zmsg_recv(source.zsock as *mut c_void) };
        if msg == ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            Ok(ZMsg { zmsg: msg })
        }
    }

    pub fn popstr(&self) -> Result<String, Error> {
        let msg_ptr = unsafe { czmq_sys::zmsg_popstr(self.zmsg) };
        if msg_ptr == ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            let msg_cstr = unsafe { CStr::from_ptr(msg_ptr).to_owned() };
            let msg_string = msg_cstr.into_string()?;
            Ok(msg_string)
        }
    }

    pub fn size(&self) -> usize {
        unsafe { czmq_sys::zmsg_size(self.zmsg) as usize }
    }

    pub fn content_size(&self) -> usize {
        unsafe { czmq_sys::zmsg_content_size(self.zmsg) as usize }
    }
}
