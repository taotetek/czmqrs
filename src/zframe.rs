extern crate czmq_sys;
extern crate libc;

use std::ffi::{CStr, CString};
use std::os::raw::c_void;
use std::{ptr, slice, str};
use Error;
use ErrorKind;
use ZSock;

bitflags! {
    pub struct Flags: i32 {
        const ZFRAME_MORE = 0b00000001;
        const ZFRAME_REUSE = 0b00000010;
        const ZFRAME_DONTWAIT = 0b00000100;
    }
}

/// Holds a CZMQ zframe_t
#[derive(Debug)]
pub struct ZFrame {
    zframe: *mut czmq_sys::zframe_t,
}

/// Clean up memory allocated from C when frame leaves scope
impl Drop for ZFrame {
    fn drop(&mut self) {
        unsafe { czmq_sys::zframe_destroy(&mut self.zframe) };
    }
}

/// Compare frames for equality
impl PartialEq for ZFrame {
    fn eq(&self, other: &ZFrame) -> bool {
        unsafe { czmq_sys::zframe_eq(self.zframe, other.zframe) == 1 }
    }
}

/// Uses zframe_dup to make a copy of the frame
impl Clone for ZFrame {
    fn clone(&self) -> ZFrame {
        let dup = unsafe { czmq_sys::zframe_dup(self.zframe) };
        ZFrame { zframe: dup }
    }
}

/// Constructs a new `Zframe`
impl ZFrame {
    pub fn new(data: &[u8]) -> ZFrame {
        let frame =
            unsafe { czmq_sys::zframe_new(data.as_ptr() as *const c_void, data.len() as u64) };
        ZFrame { zframe: frame }
    }

    pub fn new_empty() -> ZFrame {
        let frame = unsafe { czmq_sys::zframe_new_empty() };
        ZFrame { zframe: frame }
    }

    pub fn from(s: &str) -> ZFrame {
        Self::new(s.as_bytes())
    }

    pub fn reset(&self, data: &[u8]) {
        unsafe {
            czmq_sys::zframe_reset(
                self.zframe,
                data.as_ptr() as *const c_void,
                data.len() as u64,
            )
        };
    }

    /// Return pointer to frame data
    pub fn data(&self) -> &[u8] {
        unsafe {
            let data = czmq_sys::zframe_data(self.zframe);
            slice::from_raw_parts(data, self.size())
        }
    }

    /// Return frame data encoded as printable hex string, useful for 0MQ UUIDs.
    /// Caller owns the return value
    pub fn strhex(&self) -> Result<String, Error> {
        let hex_ptr = unsafe { czmq_sys::zframe_strhex(self.zframe) };
        if hex_ptr == ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            let hex_owned = unsafe { CStr::from_ptr(hex_ptr) }.to_owned();
            let hex_string = hex_owned.into_string()?;
            Ok(hex_string)
        }
    }

    /// Return frame data copied into freshly allocated string
    /// Caller owns the return value
    pub fn strdup(&self) -> Result<String, Error> {
        let char_ptr = unsafe { czmq_sys::zframe_data(self.zframe) };
        if char_ptr == ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            let char_slice = unsafe { slice::from_raw_parts(char_ptr, self.size()) };
            let char_string = String::from_utf8(char_slice.to_vec())?;
            Ok(char_string)
        }
    }

    /// Return true if frame body is equal to string
    pub fn streq(&self, string: &str) -> Result<bool, Error> {
        let c_str = CString::new(string)?;
        let rc = unsafe { czmq_sys::zframe_streq(self.zframe, c_str.as_ptr()) == 1 };
        Ok(rc)
    }

    /// Send frame to socket, destroy after sending
    pub fn send(mut self, dest: &ZSock, flags: Option<Flags>) -> Result<i32, Error> {
        let czmq_flags = if let Some(mut f) = flags {
            f.remove(Flags::ZFRAME_REUSE);
            f.bits()
        } else {
            0
        };

        let size = unsafe {
            czmq_sys::zframe_send(
                &mut self.zframe as *mut *mut czmq_sys::zframe_t,
                dest.zsock as *mut c_void,
                czmq_flags,
            )
        };

        if size == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(size)
        }
    }

    /// Send frame to socket, do not destroy after sending
    pub fn send_with_reuse(&mut self, dest: &ZSock, flags: Option<Flags>) -> Result<i32, Error> {
        let czmq_flags = if let Some(f) = flags {
            f | Flags::ZFRAME_REUSE
        } else {
            Flags::ZFRAME_REUSE
        };

        let size = unsafe {
            czmq_sys::zframe_send(
                &mut self.zframe as *mut *mut czmq_sys::zframe_t,
                dest.zsock as *mut c_void,
                czmq_flags.bits(),
            )
        };

        if size == -1 {
            Err(Error::Internal(ErrorKind::CZMQError))
        } else {
            Ok(size)
        }
    }

    /// Receive frame from socket. Blocking receive. For non blocking
    /// use ZPoller.
    pub fn recv(source: &ZSock) -> Result<ZFrame, Error> {
        let frame = unsafe { czmq_sys::zframe_recv(source.zsock as *mut c_void) };
        if frame == ::std::ptr::null_mut() {
            Err(Error::Internal(ErrorKind::NulPtrError))
        } else {
            Ok(ZFrame { zframe: frame })
        }
    }

    /// Return size of frame
    pub fn size(&self) -> usize {
        unsafe { czmq_sys::zframe_size(self.zframe) as usize }
    }

    /// Return frame MORE indicator
    pub fn more(&self) -> bool {
        unsafe { czmq_sys::zframe_more(self.zframe) == 1 }
    }

    /// Set frame MORE indicator. Note this is NOT used
    /// when sending frame to socket.
    pub fn set_more(&self, more: bool) {
        let val = if more { 1 } else { 0 };
        unsafe { czmq_sys::zframe_set_more(self.zframe, val) }
    }

    /// Return meta data property for frame. The caller shall not modify or free
    /// the returned value, which is owned by the message. Returns None if the
    /// property did not exist.
    pub fn meta(&self, property: &str) -> Result<Option<&str>, Error> {
        let c_prop = CString::new(property)?;
        let meta_ptr = unsafe { czmq_sys::zframe_meta(self.zframe, c_prop.as_ptr()) };
        if meta_ptr == ::std::ptr::null_mut() {
            Ok(None)
        } else {
            let c_str = unsafe { CStr::from_ptr(meta_ptr) };
            let meta_str = c_str.to_str()?;
            Ok(Some(meta_str))
        }
    }
}

#[cfg(test)]
mod tests {
    use {Error, Flags, ZFrame, ZSock, ZSockType};

    #[test]
    fn test_zframe_new() -> Result<(), Error> {
        let _frame = ZFrame::new("test".as_bytes());
        Ok(())
    }

    #[test]
    fn test_zframe_eq() -> Result<(), Error> {
        let frame = ZFrame::new("equal".as_bytes());
        let frame_eq = ZFrame::new("equal".as_bytes());
        let frame_neq = ZFrame::new("not equal".as_bytes());
        assert_eq!(frame, frame_eq);
        assert!(frame != frame_neq);
        Ok(())
    }

    #[test]
    fn test_zframe_clone() -> Result<(), Error> {
        let frame = ZFrame::new("equal".as_bytes());
        let clone = frame.clone();
        assert_eq!(frame, clone);
        Ok(())
    }

    #[test]
    fn test_zframe_new_empty() -> Result<(), Error> {
        let frame_empty = ZFrame::new_empty();
        assert_eq!(frame_empty.size(), 0);
        Ok(())
    }

    #[test]
    fn test_zframe_from() -> Result<(), Error> {
        let frame = ZFrame::from("from test");
        assert!(frame.streq("from test")?);
        assert!(!frame.streq("not equal")?);
        Ok(())
    }

    #[test]
    fn test_zframe_reset() -> Result<(), Error> {
        let frame = ZFrame::from("original");
        frame.reset("reset".as_bytes());
        assert!(frame.streq("reset")?);
        Ok(())
    }

    #[test]
    fn test_zframe_data() -> Result<(), Error> {
        let frame = ZFrame::from("data");
        let bytes = frame.data();
        assert_eq!("data".as_bytes(), bytes);
        Ok(())
    }

    #[test]
    fn test_zframe_strhex() -> Result<(), Error> {
        let frame = ZFrame::from("END");
        let hexstr = frame.strhex()?;
        assert_eq!(hexstr, "454E44");
        Ok(())
    }

    #[test]
    fn test_zframe_strdup() -> Result<(), Error> {
        let frame = ZFrame::from("test string");
        let dupstr = frame.strdup()?;
        assert_eq!(dupstr, "test string");
        Ok(())
    }

    #[test]
    fn test_zframe_streq() -> Result<(), Error> {
        let frame = ZFrame::from("test string");
        assert!(frame.streq("test string")?);
        assert!(!frame.streq("not equal")?);
        Ok(())
    }

    #[test]
    fn test_zframe_send_recv() -> Result<(), Error> {
        let output = ZSock::new(ZSockType::PAIR);
        let port = output.bind("tcp://127.0.0.1:*")?;

        let input = ZSock::new(ZSockType::PAIR);

        let endpoint = format!("tcp://127.0.0.1:{}", port);
        let _rc = input.connect(&endpoint)?;

        let mut flags = Flags::ZFRAME_MORE;

        for i in 0..5 {
            if i == 4 {
                flags.remove(Flags::ZFRAME_MORE);
            }

            let frame = ZFrame::from(&format!("Message {}", i));
            let _rc = frame.send(&output, Some(flags))?;
        }

        let mut frame_nbr = 0;
        loop {
            let frame = ZFrame::recv(&input)?;
            assert!(frame.streq(&format!("Message {}", frame_nbr))?);
            frame_nbr = frame_nbr + 1;
            if !frame.more() {
                break;
            }
        }
        Ok(())
    }

    #[test]
    fn test_zframe_send_with_reuse_recv() -> Result<(), Error> {
        let output = ZSock::new(ZSockType::PAIR);
        let port = output.bind("tcp://127.0.0.1:*")?;

        let input = ZSock::new(ZSockType::PAIR);

        let endpoint = format!("tcp://127.0.0.1:{}", port);
        let _rc = input.connect(&endpoint)?;

        let mut flags = Flags::ZFRAME_MORE;

        let mut frame = ZFrame::from("Reusable");
        for i in 0..5 {
            if i == 4 {
                flags.remove(Flags::ZFRAME_MORE);
            }
            let _rc = frame.send_with_reuse(&output, Some(flags))?;
        }

        let mut frame_nbr = 0;
        loop {
            let frame = ZFrame::recv(&input)?;
            assert!(frame.streq("Reusable")?);
            frame_nbr = frame_nbr + 1;
            if !frame.more() {
                break;
            }
        }
        Ok(())
    }

    #[test]
    fn test_zframe_more() -> Result<(), Error> {
        let frame = ZFrame::new_empty();
        assert!(!frame.more());
        frame.set_more(true);
        assert!(frame.more());
        frame.set_more(false);
        assert!(!frame.more());
        Ok(())
    }
}
