extern crate czmqrs;

use std::{thread, time};

fn main() -> Result<(), czmqrs::Error> {
    println!("Starting hello world server...\n");

    let responder = czmqrs::ZSock::new(czmqrs::ZSockType::REP);
    let _rc = responder.bind("tcp://*:5555")?;

    loop {
        let frame = czmqrs::ZFrame::recv(&responder)?;
        println!("Received: {}", frame.strdup()?);

        match frame.streq("Hello")? {
            true => {
                thread::sleep(time::Duration::from_millis(1000));

                let frame = czmqrs::ZFrame::from("World");
                let _rc = frame.send(&responder, None)?;
            }
            false => {
                break;
            }
        }
    }

    Ok(())
}
