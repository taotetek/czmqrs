extern crate czmqrs;

fn main() -> Result<(), czmqrs::Error> {
    println!("Connecting to hello world server...\n");

    let requestor = czmqrs::ZSock::new(czmqrs::ZSockType::REQ);
    let _rc = requestor.connect("tcp://localhost:5555")?;

    for request_nbr in 0..10 {
        let frame = czmqrs::ZFrame::from("Hello");
        let _rc = frame.send(&requestor, None)?;

        let frame = czmqrs::ZFrame::recv(&requestor)?;
        println!("Received World {}: {}", frame.strdup()?, request_nbr);
    }

    let frame = czmqrs::ZFrame::from("Bye");
    let _rc = frame.send(&requestor, None)?;

    Ok(())
}
